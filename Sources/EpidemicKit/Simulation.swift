//
//  Simulation.swift
//  
//
//  Created by Jeremy Pereira on 17/05/2020.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Statistics
import Toolbox

/// Models a simulation of the spread of a virus
public struct Simulation<RNG: RandomNumberGenerator>
{
	/// The population of this simulation
	public private(set) var population: Population
	/// Characteristics of the virus
	public let virus: Virus

	private var statistics: Statistics<Population>
	private var rng: RNG

	/// Create a new simulation
	/// - Parameters:
	///   - initialPopulation: The population at the start of the simulation
	///   - virus: The virus characteristics of the simulation
	///   - statsFile: Where to put the results
	///   - dataSet: Name of the dataset recorded by the `Statistics`. If `nil`
	///              defaults to `"Population"`
	///   - rng: The random number generator to be used by this simulation
	/// - Throws: If we have a problem ith the stats file
	public init(initialPopulation: Population,
							virus: Virus,
						statsFile: String,
						  dataSet: String = "Population",
							  rng: RNG) throws
	{
		self.population = initialPopulation
		self.virus = virus
		statistics = try Statistics<Population>(sqlite3File: statsFile, dataSetName: dataSet)
		try statistics.record(dataPoint: population)
		self.rng = rng
	}

	/// Calculate the next day's population
	/// - Throws: if we cannot record the new statistic
	public mutating func calculateNextDay() throws
	{
		let totalInfected = population.infected.count
		let totalPopulation = population.susceptible + totalInfected + population.recovered
		var stillInfected: [InfectedPerson] = []
		var rData = Population.RDataPoint.zero
		var newRecoveredCount = 0
		var currentSusceptible = population.susceptible
		for person in population.infected
		{
			if virus.didRecover(person: person)
			{
				newRecoveredCount += 1
				rData += Population.RDataPoint(infector: 1, infectee: Double(person.othersInfected))
			}
			else
			{
				var infectedCount = 0
				// Loop through the random people met seeing if they got infected
				let peopleMet = person.isSymptomatic ? population.isolating : population.sociability
				for _ in 0 ..< peopleMet
				{
					if (0 ..< totalPopulation).randomElement(using: &rng) ?? Int.max < currentSusceptible
					{
						// We have met a susceptible person
						if Double.random(in: 0 ... 1, using: &rng) <= virus.infectionProbability
						{
							// And we have infected them
							currentSusceptible -= 1
							infectedCount += 1
						}
					}
				}
				// Make the new infectees
				let newInfectees = Array<InfectedPerson>(repeating: InfectedPerson(isSymptomatic: virus.asymptomatic == 0), count: infectedCount)
				stillInfected += newInfectees
				stillInfected.append(InfectedPerson(daysInfected: person.daysInfected + 1,
													othersInfected: person.othersInfected + infectedCount,
													isSymptomatic: person.daysInfected + 1 > virus.asymptomatic))
			}
		}
		let newRecovered = population.recovered + newRecoveredCount
		population = Population(susceptible: currentSusceptible,
								infected: stillInfected,
								recovered: newRecovered,
								infectionsForR: population.infectionsForR.adding(value: rData),
								sociability: population.sociability,
								isolating: population.isolating)
		try statistics.record(dataPoint: population)
	}
}
