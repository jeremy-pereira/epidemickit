//
// Copyright © Jeremy Pereira 2020
//
// EpidemicKit.swift
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import Foundation

private let log = Logger.getLogger("EpidemicKit.EpidemicKit")


/// An epidemic
public struct EpidemicKit
{
	public init() {}

	/// Simulate an epidemic
	/// - Parameters:
	///   - virus: The type of virus to use
	///   - population: The initial population
	///   - days: Number of days to simulate for
	///   - database: Name of the database in which to record the results
	///   - dataSet: Name of the dataset within the database in which to record the results
	/// - Throws: If anything goes wrong with the simulation
	public func simulate(virus: Virus, population: Population, days: Int, database: String, dataSet: String) throws
	{
		log.info("Where am I? \(FileManager.default.currentDirectoryPath)")

		var simulation = try Simulation(initialPopulation: population,
										virus: virus,
										statsFile: database,
										dataSet: makeDataSetName(prefix: dataSet),
										rng: SystemRandomNumberGenerator())
		for day in 0 ..< days
		{
			try simulation.calculateNextDay()
			guard simulation.population.infected.count > 0
			else
			{
				log.info("Disease died out on day \(day)")
				break
			}
			if day.isMultiple(of: 10)
			{
				log.info("Computed \(day) days")
			}
		}
	}

	private func makeDataSetName(prefix: String) -> String
	{
		let date = Date()
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy_MM_dd_hhmmss"
		let string = formatter.string(from: date)
		return prefix + "_" + string
	}
}
