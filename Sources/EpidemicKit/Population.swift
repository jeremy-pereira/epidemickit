//
//  Population.swift
//  
//
//  Created by Jeremy Pereira on 17/05/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Statistics
import SQL

/// Models a population
///
///
public struct Population
{
	/// The number of people who have not yet had the disease and are vulnerable
	public let susceptible: Int
	/// The peple currently infected.
	///
	public let infected: [InfectedPerson]
	/// The number of people who have recovered.
	public let recovered: Int
	/// The number of infections used to calculate R
	///
	/// This is the number of infections caused by people who have recovered.
	/// This is used for calculating R. It's the cumulative infections per
	/// infected person who has recovered.
	public let infectionsForR: RollingAverage<SIMD2<Double>>

	/// How many other people each person will meet in a day
	public let sociability: Int
	/// How many people does somebody known to be infected meet every day.
	public let isolating: Int

	/// Initialise a population
	///
	/// - Parameters:
	///   - susceptible: The number of susceptible people in the population
	///   - infected: The number of infected people broken down by days infected.
	///               The default is to have nobody infected.
	///   - recovered: The number of people who have recovered. The default is
	///                to have nobody recovered.
	///   - infectionsForR: The rolling sum over one week  of infections caused by
	///                     everybody who has recovered.
	///   - sociability: How many people does each person meet in a day
	///   - isolating: How many people does an infected person who is symptomatic
	///                meet every day.
	public init(susceptible: Int,
				infected: [InfectedPerson] = [],
				recovered: Int = 0,
				infectionsForR: RollingAverage<SIMD2<Double>>,
				sociability: Int,
				isolating: Int)
	{
		self.susceptible = susceptible
		self.infected = infected
		self.recovered = recovered
		self.sociability = sociability
		self.infectionsForR = infectionsForR
		self.isolating = isolating
	}

	/// Initialise a new population with no infection history
	///
	/// - Parameters:
	///   - susceptible: Number of susceptible people in the population
	///   - infected: Number of infected people in the population. Defaults to `1`
	///   - isSymptomatic: Are the starting infected persons sympromatic, defaults to `true`
	///   - rRollingPeriod: Period over which to calculate rolling R value
	///   - sociability: Sociability of people in the population
	///   - isolating: How many people does an infected person who is symptomatic
	///                meet every day. If it's nil (the default) it is the same
	///                as sociability i.e. an infected person does not self
	///                isolate
	public init(susceptible: Int,
				infected: Int = 1,
				isSymptomatic: Bool = true,
				rRollingPeriod: Int,
				sociability: Int,
				isolating: Int? = nil)
	{
		self.init(susceptible: susceptible,
				  infected: Array<InfectedPerson>(repeating: InfectedPerson(isSymptomatic: isSymptomatic), count: infected),
				  infectionsForR: RollingAverage<SIMD2<Double>>(period: rRollingPeriod),
				  sociability: sociability,
				  isolating: isolating ?? sociability)
	}

	public var r: Double?
	{
		let rSum = infectionsForR.sum
		return rSum.infector == 0 ? nil : rSum.infectee / rSum.infector
	}

	public typealias RDataPoint = SIMD2<Double>
}

extension Population: Recordable
{
	public var data: [String : StatsMappable]
	{
		let symptomatic = symptomaticCount()
		let ret: [String : StatsMappable] = [
			"susceptible" : susceptible,
			"recovered" : recovered,
			"infected_a" : infected.count - symptomatic,
			"infected_s" : symptomatic,
			"r" : r
		]
		return ret
	}

	private func symptomaticCount() -> Int
	{
		var count = 0
		for p in infected
		{
			if p.isSymptomatic
			{
				count += 1
			}
		}
		return count
	}
}

extension SIMD2: Averagable where SIMD2.Scalar: FloatingPoint
{
	public func dividedBy(int: Int) -> SIMD2<Scalar>
	{
		return self / Scalar(int)
	}
}

public extension Population.RDataPoint
{

	/// Initialsise an r data point
	/// - Parameters:
	///   - infector: The number of infectors
	///   - infectee: The number of infectees
	init(infector: Double, infectee: Double)
	{
		self.init([infector, infectee])
	}
	/// The infector count
	var infector: Double
	{
		get { self[0] }
		set { self[0] = newValue }
	}
	/// The infectee count
	var infectee: Double
	{
		get { self[1] }
		set { self[1] = newValue }
	}
}
