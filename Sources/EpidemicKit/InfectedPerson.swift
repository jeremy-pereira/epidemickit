//
//  InfectedPerson.swift
//  
//
//  Created by Jeremy Pereira on 19/05/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// Models an infected person
public struct InfectedPerson
{
	/// How many days before this person recovers
	public let daysInfected: Int
	/// How many other people has this person infected.
	public let othersInfected: Int
	/// Is the infected person symptomatic
	public let isSymptomatic: Bool

	/// Create a new infected person
	/// - Parameters:
	///   - daysToRecovery: How many days before this person will recover
	///   - othersInfected: How many other people has this person infected.
	///   - isSymptomatic: Is this person symptomatic, deaults to `true`
	public init(daysInfected: Int = 0, othersInfected: Int = 0, isSymptomatic: Bool = true)
	{
		self.daysInfected = daysInfected
		self.othersInfected = othersInfected
		self.isSymptomatic = isSymptomatic
	}
}
