//
//  Virus.swift
//  
//
//  Created by Jeremy Pereira on 17/05/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// Protocol defining charcteristics of a virus
public protocol Virus
{
	/// Decides whether a person infected did recover or not
	///
	/// This function can use a source of randomness if necessary. An extension
	/// method exists: `didRecover(person: InfectedPerson)` that calls this
	/// method with the system random number generator
	///
	///
	/// - Parameters:
	///   - daysInfected: Number of days a person was infected
	///   - rng: A random number generator used if randomness is needed.
	/// - Returns: `true` if the person recovered
	func didRecover<RNG: RandomNumberGenerator>(person: InfectedPerson,
												using rng: inout RNG) -> Bool

	/// Probability of infecting a person you meet
	///
	/// This must be a number in the range `0 ... 1`
	var infectionProbability: Double { get }
	/// Number of days for which the victim is infectious but asymptomatic
	var asymptomatic: Int { get }
}

extension Virus
{
	func didRecover(person: InfectedPerson) -> Bool
	{
		var rng = SystemRandomNumberGenerator()
		return didRecover(person: person, using: &rng)
	}
}

/// The characteristics of a simple virus where you recover after a set number
/// of days.
public struct SimpleVirus: Virus
{
	/// Probability of an infected person infecting a susceptible person if they
	/// meet.
	public let infectionProbability: Double
	/// Number of days it takes to recover from this virus
	private let daysToRecover: Int
	/// Number of days for which the victim is infectious but asymptomatic
	public let asymptomatic: Int

	public func didRecover<RNG: RandomNumberGenerator>(person: InfectedPerson,
													   using rng: inout RNG) -> Bool
	{
		return person.daysInfected >= daysToRecover
	}

	/// Initialise a new virus
	///
	/// - Parameters:
	///   - infectionProbability: The probability that a person an infected
	///                           person meets gets infected
	///   - asymptomatic: The number of days a person is infectious but
	///                   asymptomatic
	///   - symptomatic: Number of days a person is infectious and symptomatic
	public init(infectionProbability: Double, asymptomatic: Int = 0, symptomatic: Int)
	{
		guard (0 ... 1).contains(infectionProbability)
			else { fatalError("Infection probability \(infectionProbability) must be in the range 0 ... 1") }
		self.infectionProbability = infectionProbability
		guard asymptomatic + symptomatic > 0
			else { fatalError("Must be infectious for at least one day") }
		self.daysToRecover = asymptomatic + symptomatic
		self.asymptomatic = asymptomatic
	}
}

/// A virus where recovery is a stochastic process.
///
/// An array of probabilities exists giving the probability of recovery on a
/// particular day *after the person starts showing symptoms*. So, if you have
/// `[0.5, 0.8]`, on the first day after shoing symptoms, the person has a 50%
/// chance of recovery, on the second day, the person has an 80% chance of
/// recovery. Days that would index after the end of the array are assumed to
/// give a 100% chance of recovery.
public struct NonDeterministicRecoveryVirus: Virus
{
	public func didRecover<RNG: RandomNumberGenerator>(person: InfectedPerson,
													   using rng: inout RNG) -> Bool
	{
		guard person.isSymptomatic else { return false }
		let dayIndex = person.daysInfected - asymptomatic
		guard dayIndex < recoveryProbabilities.count else { return true }
		return Double.random(in: 0 ... 1, using: &rng) <= recoveryProbabilities[dayIndex]
	}

	public let infectionProbability: Double

	public let asymptomatic: Int

	public let recoveryProbabilities: [Double]

	/// Initialise a new virus
	///
	/// - Parameters:
	///   - infectionProbability: The probability that a person an infected
	///                           person meets gets infected
	///   - asymptomatic: The number of days a person is infectious but
	///                   asymptomatic
	///   - recoveryProbabilities: The probability a person will recover on a particular day
	///                            after they present as symptomatic.
	public init<S: Collection>(infectionProbability: Double, asymptomatic: Int = 0, recoveryProbabilities: S)
		where S.Element == Double
	{
		guard (0 ... 1).contains(infectionProbability)
			else { fatalError("Infection probability \(infectionProbability) must be in the range 0 ... 1") }
		self.infectionProbability = infectionProbability
		guard asymptomatic >= 0
			else { fatalError("Must be infectious for at least one day") }
		self.recoveryProbabilities = recoveryProbabilities.map
		{
			guard (0 ... 1).contains($0) else { fatalError("Probability must be in the closed range 0 ... 1")}
			return $0
		}
		self.asymptomatic = asymptomatic
	}

}
