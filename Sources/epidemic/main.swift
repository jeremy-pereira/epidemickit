//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 18/05/2020.
//
import EpidemicKit
import Foundation
import Toolbox
import ArgumentParser

let log = Logger.getLogger("epidemic.main")

log.level = .info
Logger.set(level: .info, forName: "EpidemicKit.EpidemicKit")

struct Program: ParsableCommand
{

	@Option(help: "An SQLite3 database into which the ouput is dumped (will be overwritten).")
	var output: String
	@Flag(help: "Run a simple test with no asymptomatic period")
	var simple: Bool
	@Flag(help: "Run a test with an asymptomatic period")
	var asymptomatic: Bool
	@Flag(help: "Use non deterministic recovery times")
	var ndr: Bool

	func run()
	{
		do
		{
			if simple
			{
				log.info("Simple run starting")
				let timeToRun = try timedRun { try simpleRun(runDb: output) }
				log.info("Simple simulation took \(timeToRun) seconds")
			}
			if asymptomatic
			{
				log.info("Asymptomatic run starting")
				let timeToRun = try timedRun { try asymptomaticRun(runDb: output) }
				log.info("Asymptomatic simulation took \(timeToRun) seconds")
			}
		}
		catch
		{
			log.error("\(error)")
		}
	}

	private func timedRun(_ block: () throws -> ()) rethrows -> Double
	{
		let start = DispatchTime.now()
		try block()
		let end = DispatchTime.now()
		let nanoseconds = end.uptimeNanoseconds - start.uptimeNanoseconds
		return Double(nanoseconds) / 1e9
	}

	let asymptomaticDays = 5

	let recoveryProbabilities =
	[
		0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.001, 0.007,
		0.013, 0.025, 0.032, 0.040, 0.048, 0.060, 0.074, 0.086, 0.098, 0.109,
		0.124, 0.127, 0.136, 0.149, 0.165, 0.170, 0.181, 0.203, 0.226, 0.236,
		0.235, 0.242, 0.255, 0.257, 0.231, 0.250, 0.200, 0.167, 0.200, 0.250,
		0.167, 0.200, 0.250, 0.333, 0.500, 1.000,
	]

	private func simpleRun(runDb: String) throws
	{
		let virus: Virus
		if ndr
		{
			// Add five days of non recovery to align with the asymptomatic case
			virus = NonDeterministicRecoveryVirus(infectionProbability: 0.01,
												  asymptomatic: 0,
												  recoveryProbabilities: Array<Double>(repeating: 0, count: asymptomaticDays) + recoveryProbabilities)
		}
		else
		{
			virus = SimpleVirus(infectionProbability: 0.01, symptomatic: 21 + asymptomaticDays) // Add five to make total time infected same as asymptomatic run
		}
		let population  = Population(susceptible: 99999, infected: 1, rRollingPeriod: 7, sociability: 20)
		try EpidemicKit().simulate(virus: virus,
								   population: population,
								   days: 180,
								   database: runDb,
								   dataSet: "SimpleRun" + (ndr ? "_ndr" : ""))
	}

	private func asymptomaticRun(runDb: String) throws
	{
		let virus: Virus
		if ndr
		{
			virus = NonDeterministicRecoveryVirus(infectionProbability: 0.01,
												  asymptomatic: asymptomaticDays,
												  recoveryProbabilities: recoveryProbabilities)
		}
		else
		{
			virus = SimpleVirus(infectionProbability: 0.01, asymptomatic: asymptomaticDays, symptomatic: 21)
		}

		let population  = Population(susceptible: 99999, infected: 1, isSymptomatic: false, rRollingPeriod: 7, sociability: 20, isolating: 3)
		try EpidemicKit().simulate(virus: virus,
								   population: population,
								   days: 360,
								   database: runDb,
								   dataSet: "AsymptomaticRun" + (ndr ? "_ndr" : ""))
	}
}

Program.main()

