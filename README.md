# EpidemicKit

A naive simulation of the growth of an epidemic.

## How To Build
This is a Swift package. Clone the repository and open it with Xcode and it should be fine.

There is a target called `epidemic` that is a command line interface to the simulation. Here is how to use it:

### Usage
```
epidemic --output <output> [--simple] [--asymptomatic]
```

|Option             |Description|
|------             |-----------|
|`--output <output>`|An SQLite3 database into which the ouput is dumped (will be overwritten). |
|`--simple`         |Run a simple test with no asymptomatic period|
|`--asymptomatic`   |Run a test with an asymptomatic period|
|`-h`, `--help`     |Show help information.|
 

## Design Decisions

Our basic time unit is one day

## Model

### Simplistic

The initial model was a simplistic one in which everybody who caught the diease recovered after exactly 15 days.

### Asymptomatic

The asymptomatic model is based on an asymptomatic period of five days followed by a sympromatic period of ten days with reduced social contact

### Variable Recovery Times 

https://www.thelancet.com/journals/laninf/article/PIIS1473-3099(20)30287-5/fulltext 