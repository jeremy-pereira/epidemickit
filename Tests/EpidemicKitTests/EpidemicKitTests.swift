//
// Copyright © Jeremy Pereira 2020
//
// EpidemicKitTests.swift
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Foundation
import Toolbox
import XCTest
import SQL
import EpidemicKit
import Statistics

private let log = Logger.getLogger("EpidemicKitTests.EpidemicKitTests")

final class EpidemicKitTests: XCTestCase
{
	let initialiseDb = "testInit.sqlite3"
	let oneDayDb = "testOneDay.sqlite3"
	let recoveryDb = "recoveryDb.sqlite3"
	let fullRunDb = "fullRunDb.sqlite3"

    override func setUp()
	{
		let fm = FileManager.default
		log.pushLevel(.info)
		log.info("Where am I? \(fm.currentDirectoryPath)")
		// Delete the DB that is not supposed to exist
		deleteFile(name: initialiseDb)
		deleteFile(name: oneDayDb)
		deleteFile(name: recoveryDb)
    }

    func testInitialise()
	{
		let virus = SimpleVirus(infectionProbability: 1, symptomatic: 10)
		let population  = Population(susceptible: 999999, infected: 1, rRollingPeriod: 7, sociability: 1)
		do
		{
			_ = try Simulation(initialPopulation: population, virus: virus, statsFile: initialiseDb, rng: SystemRandomNumberGenerator())
			let connection = SQLLite3Connection(fileName: initialiseDb)
			try connection.open()
			defer { connection.close() }
			let statement = Statement.select(columnNames: ["susceptible", "infected_s", "recovered"],
											 from: TableName("population"))
			try connection.with(statement: statement)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No initial result") ; return }
				guard case SQLValue.integer(let susceptible)? = firstResult["susceptible"]
					else { XCTFail("No isusceptible column") ; return }
				XCTAssert(susceptible == 999999)
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }


    func testOneDay()
	{
		let virus = SimpleVirus(infectionProbability: 1, symptomatic: 10)
		let population  = Population(susceptible: 999999, infected: 1, rRollingPeriod: 7, sociability: 2)
		do
		{
			var simulation = try Simulation(initialPopulation: population, virus: virus, statsFile: oneDayDb, rng: SystemRandomNumberGenerator())
			try simulation.calculateNextDay()
			let connection = SQLLite3Connection(fileName: oneDayDb)
			try connection.open()
			defer { connection.close() }
			let statement = Statement.select(columnNames: ["susceptible", "infected_s", "recovered"],
											 from: TableName("population"))
			try connection.with(statement: statement)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No initial result") ; return }
				guard case SQLValue.integer(let infected1)? = firstResult["infected_s"]
					else { XCTFail("No infected column") ; return }
				XCTAssert(infected1 == 1)
				guard let result2 = try results.next()
					else { XCTFail("No second result") ; return }
				guard case SQLValue.integer(let infected2)? = result2["infected_s"]
					else { XCTFail("No infected column") ; return }
				XCTAssert(infected2 == 3, "Wrong infeted count \(infected2)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

	func testRecovery()
	{
		let virus = SimpleVirus(infectionProbability: 0, symptomatic: 5)
		let infectedPeople =
		[
			InfectedPerson(daysInfected: 1),
			InfectedPerson(daysInfected: 2),
			InfectedPerson(daysInfected: 3),
			InfectedPerson(daysInfected: 4),
			InfectedPerson(daysInfected: 4),
			InfectedPerson(daysInfected: 5),
			InfectedPerson(daysInfected: 5),
		]
		let population  = Population(susceptible: 999993,
									 infected: infectedPeople,
									 infectionsForR: RollingAverage<Population.RDataPoint>(period: 7),
									 sociability: 2,
									 isolating: 2)
		do
		{
			var simulation = try Simulation(initialPopulation: population, virus: virus, statsFile: recoveryDb, rng: SystemRandomNumberGenerator())
			try simulation.calculateNextDay()
			XCTAssert(simulation.population.recovered == 2, "Wrong number recovered: \(population.recovered)")
			XCTAssert(simulation.population.susceptible == 999993)
			let totalInfected = simulation.population.infected.count
			XCTAssert(totalInfected == 5)
			try simulation.calculateNextDay()
			XCTAssert(simulation.population.recovered == 4, "Actuall recovered == \(simulation.population.recovered)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	private func deleteFile(name: String)
	{
        var dbBytes = try! name.signedBytes(encoding: String.Encoding.utf8)
        dbBytes.append(0)
        unlink(&dbBytes)
	}


    static var allTests = [
        ("testInitialise", testInitialise),
		("testOneDay", testOneDay),
		("testRecovery", testRecovery),
    ]
}
